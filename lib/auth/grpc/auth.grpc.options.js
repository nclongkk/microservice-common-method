"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthServiceGrpcOptions = void 0;
var microservices_1 = require("@nestjs/microservices");
var auth_constant_1 = require("./../constants/auth.constant");
exports.AuthServiceGrpcOptions = {
    transport: microservices_1.Transport.GRPC,
    options: {
        url: auth_constant_1.AUTH_HOST,
        package: auth_constant_1.AUTH_PACKAGE_NAME,
        protoPath: "node_modules/microservice-common-method/src/" + auth_constant_1.AUTH_PACKAGE_NAME + "/grpc/" + auth_constant_1.AUTH_PACKAGE_NAME + ".proto",
    },
};
