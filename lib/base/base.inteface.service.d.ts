interface IPopulate {
    path: string;
    select?: string;
    populate?: IPopulate;
    match?: any;
    options?: any;
}
export interface IParamQuery {
    ignoreCheckLimit?: boolean;
    page?: number;
    limit?: number;
    sort?: Record<string, number> | string;
    isLean?: boolean;
    populate?: IPopulate[];
    skip?: number;
    select?: string;
    where?: any;
    data?: any;
    options?: Record<string, unknown>;
    filter?: any;
}
export interface IBaseService {
    getAll(params: IParamQuery): Promise<any[]>;
    getOne(params: IParamQuery): Promise<any>;
    updateOne(params: IParamQuery): Promise<any>;
    createOne(params: IParamQuery): Promise<any>;
    deleteOne(params: IParamQuery): Promise<any>;
    deleteMany(params: IParamQuery): Promise<any>;
    count(params: IParamQuery): Promise<number>;
    findOneAndUpdate(params: IParamQuery): Promise<any>;
    findOneAndDelete(params: IParamQuery): Promise<any>;
}
export {};
