"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TtmServiceGrpcOptions = void 0;
var microservices_1 = require("@nestjs/microservices");
var ttm_constants_1 = require("./../constants/ttm.constants");
exports.TtmServiceGrpcOptions = {
    transport: microservices_1.Transport.GRPC,
    options: {
        url: ttm_constants_1.TTM_HOST,
        package: ttm_constants_1.TTM_PACKAGE_NAME,
        protoPath: "node_modules/microservice-common-method/src/" + ttm_constants_1.TTM_PACKAGE_NAME + "/grpc/" + ttm_constants_1.TTM_PACKAGE_NAME + ".proto",
    },
};
