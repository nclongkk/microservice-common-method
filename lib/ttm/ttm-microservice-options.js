"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TtmMicroserviceOptions = void 0;
var microservices_1 = require("@nestjs/microservices");
exports.TtmMicroserviceOptions = {
    transport: microservices_1.Transport.GRPC,
    options: {
        package: "ttm",
        protoPath: "node_modules/microservice-common-method/src/ttm/ttm.proto",
    },
};
