import { Observable } from "rxjs";

export interface IAuthGrpcService {
  getUserByLocalUserId(
    localUserId: LocalUserId
  ): Promise<any> | Observable<any>;
  getUser(query: Query): Promise<any> | Observable<any>;
  getUsers(query: Query): Promise<any> | Observable<any>;
}

export interface LocalUserId {
  localUserId: number;
}

export interface Query {
  query: string;
}
