import { ClientOptions, Transport } from "@nestjs/microservices";
import { AUTH_HOST, AUTH_PACKAGE_NAME } from "./../constants/auth.constant";

export const AuthServiceGrpcOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: AUTH_HOST,
    package: AUTH_PACKAGE_NAME,
    protoPath: `node_modules/microservice-common-method/src/${AUTH_PACKAGE_NAME}/grpc/${AUTH_PACKAGE_NAME}.proto`,
  },
};
