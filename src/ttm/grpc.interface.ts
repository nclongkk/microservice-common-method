import { Observable } from "rxjs";

export interface ITtmGrpcService {
  accumulate(numberArray: INumberArray): ISumOfNumberArray | Observable<any>;
}

export interface INumberArray {
  data: number[];
}

export interface ISumOfNumberArray {
  sum: number;
}
