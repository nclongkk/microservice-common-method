import { Observable } from "rxjs";

export interface ITtmGrpcService {
  getUserByLocalUserId(query: number): Promise<any> | Observable<any>;
  getUser(query: string): Promise<any> | Observable<any>;
  getUsers(query: string): Promise<any> | Observable<any>;
  getShopifyStoreOfUser(
    req: IReqGetShopifyStoreOfUser
  ): Promise<IResGetShopifyStoreOfUser> | Observable<IResGetShopifyStoreOfUser>;
}

export interface IReqGetShopifyStoreOfUser {
  localUserId: number;
  shopifyUrl: string;
}

export interface IResGetShopifyStoreOfUser {
  shopifyUrl: string;
  accessToken: string;
  email: string;
}
