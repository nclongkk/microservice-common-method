import { ClientOptions, Transport } from "@nestjs/microservices";
import { TTM_HOST, TTM_PACKAGE_NAME } from "./../constants/ttm.constants";

export const TtmServiceGrpcOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: TTM_HOST,
    package: TTM_PACKAGE_NAME,
    protoPath: `node_modules/microservice-common-method/src/${TTM_PACKAGE_NAME}/grpc/${TTM_PACKAGE_NAME}.proto`,
  },
};
