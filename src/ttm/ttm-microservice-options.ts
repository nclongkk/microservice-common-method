import { ClientOptions, Transport } from "@nestjs/microservices";

export const TtmMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: "ttm",
    protoPath: "node_modules/microservice-common-method/src/ttm/ttm.proto",
  },
};
